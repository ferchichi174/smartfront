import React, { useState, useEffect } from 'react';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useHistory } from "react-router-dom";
import img_default from '../../../assets/img/img_default.png';
import { getStringOnlyDate } from '../../../utils/helpers/date.helper';



const ClientDetails = ({ showP, setShowP, data }) => {


    const renderEtat = (etat) => {
        if (etat == "active")
            return <span class="badge bg-success">Active</span>;
        else if (etat == "pending")
            return <span class="badge bg-warning text-dark">En attente</span>;
        else
            return <span class="badge bg-secondary">{etat}</span>
    }



    return (
        <>
            <div className={showP ? "col-md-5 bg-gray inf_box_pop_r" : "col-md-5 bg-gray inf_box_pop_r inf_box_pop_r_off"}>
                <div className="row no-padd no-marg bg-gray">
                    <button className="btn_close" style={{ textAlign: "left", background: "#212b60" }} onClick={() => setShowP(false)}><FontAwesomeIcon icon={['fas', 'chevron-right']} /></button>
                </div>

                <div className="row no-marg">
                    <div className="col-md-12 bg-wt">
                        <div className="row no-marg pt-5 pb-3">


                            <div className="col-md-2 text-end">
                                {data && data.user && data.user.img ?
                                    <img className="btn_img_pick_src position-relative" style={{ width: "100px", height: "100px" }} src={data.user.img} alt="" />
                                    :
                                    <img className="btn_img_pick_src position-relative" style={{ width: "100px", height: "100px" }} src={img_default} alt="" />

                                }

                            </div>

                            <div className="col-md-10 ps-4">
                                <h5 className="txt-purple mb-0 mt-5">{data && data.nom_entreprise} </h5>
                                <b className="d-block">Type : <span>{data && data.type_client} </span> </b>
                                <b>Etat : {data && data.user && renderEtat(data.user.etat)} </b>
                            </div>

                           


                            <div className="col-md-12  dvdr-t-gray pt-4 pb-4 mt-2">

                                <div className="row no-marg">
                                    <div className="col-md-12 ">
                                        <b className="d-block fnt-w5 txt-purple mb-2">Info de l'entreprise </b>
                                        <b className="d-block fnt-w5 txt-gray">date de creation <span className="txt_blk ps-3">{data && data.date_creation}</span> </b>
                                        <b className="d-block fnt-w5 txt-gray">Secteur d'activité <span className="txt_blk ps-3">{data && data.secteur}</span> </b>
                                        <b className="d-block fnt-w5 txt-gray">Site web <a href={`http://${data && data.site_web}`} target="_blank">{data && data.site_web}</a> </b>

                                        <b className="d-block fnt-w5 txt-purple mt-2">Présentation </b>
                                        <p className="d-block txt_blk">{data && data.resume} </p>
                                    </div>
                                    <div className="col-md-12">
                                        <b className="d-block fnt-w5 txt-purple mb-2">Info du compte </b>
                                        <b className="d-block fnt-w5 txt-gray">Nom <span className="txt_blk ps-3">{data && data.user && data.user.nom}</span> </b>
                                        <b className="d-block fnt-w5 txt-gray">Prénom <span className="txt_blk ps-3">{data && data.user && data.user.prenom}</span> </b>
                                        <b className="d-block fnt-w5 txt-gray">E-mail <span className="txt_blk ps-3">{data && data.user && data.user.email}</span> </b>

                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>



            </div>
        </>

    );
};

export default ClientDetails;