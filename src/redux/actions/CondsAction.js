import {
    CLEAR_CONDS ,
    LOAD_CONDS , LOAD_CONDS_SUCCESS , LOAD_CONDS_FAIL ,
} from "./ActionTypes";

import service_adm from '../../services/api/AdmApiCall';



export const clear_conds = () => {
    return (dispatch) => {
        dispatch({
            type: CLEAR_CONDS,
        });
    }
}

export const getCondidatList = () => {

    return (dispatch) => {
        dispatch({
            type: LOAD_CONDS,
        });

        service_adm.getAllCond().then(
            (res) => {
                if(res.data)
                    dispatch({
                        type: LOAD_CONDS_SUCCESS,
                        payload: res.data
                    });
                else
                    dispatch({
                        type: LOAD_CONDS_FAIL,
                        payload: 'Echec de récupération de données !'
                    });
                

            }, (error) => {
                const message = (error.response && error.response.data && error.response.data.message) || error.message || error.toString();
                dispatch({
                    type: LOAD_CONDS_FAIL,
                    payload: message
                });
            }
        );
    }

}