import React from 'react';
import CondItem from '../items/CondItem';

const CondidatsList = ({loading ,lst_data , onItemDetailsClick}) => {
    return (
        <div className="row no-marg bg-wt">

            {loading ?
                <div class="col-md-12 p-4 txt-ctr">
                    <div class="spinner-border" role="status">
                        <span class="sr-only">Loading...</span>
                    </div>
                </div>
                :
                lst_data.map((s , index) => <CondItem key={`cditm_.${index}`} data={s} onItemDetailsClick={onItemDetailsClick} />)
            }

        </div>
    );
};

export default CondidatsList;