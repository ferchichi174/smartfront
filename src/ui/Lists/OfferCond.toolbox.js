import React, { useState } from 'react';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import offer_banner from '../../assets/img/offer_banner.svg'
import Form from 'react-bootstrap/Form';

const OfferCondToolbox = ({ dataCount, onSearch, onFilter }) => {

    const [search, setSearch] = useState("")
    const [last_search, set_last_search] = useState("")

    const onClear = () => {
        setSearch("");
        set_last_search("")
        onSearch("");
    }

    const onSubmit = (s) => {
        set_last_search(s)
        onSearch(s)
        setSearch("")
    }

   

    return (
        <div className="col-md-12 banner_box no-padd mb-3">
            <img src={offer_banner} className="banner_box_img" alt="" style={{ width: "20%", top: "-40%" }} />
            <div className="col-md-12 banner_box_effect"></div>
            <div className="col-md-12 banner_box_content">
                <div className="row nom-marg">
                    <div className="col-md-12">
                        <h4 className="txt-purple2 fnt-larg"><FontAwesomeIcon icon={['fas', 'briefcase']} /> Les offres <span className="txt-purple2"> ({dataCount})</span>  </h4>
                        
                    </div>
                    <div className="col-md-6 offset-md-3  mb-3 ">

                        <input type="text" onChange={e => setSearch(e.target.value)} value={search} style={{ boxShadow: "-1px 0px 5px rgb(0 0 0 / 23%)" }} placeholder="Recherche ...." className="txt-ctr form-control inpt_search form-control bg-wt" onKeyPress={(e) => { e.key === 'Enter' && onSubmit(e.target.value); }} />
                        <button className="btn btn_search" onClick={e => onSubmit(search)}><FontAwesomeIcon icon={['fas', 'search']} /> </button>
                    </div>
                  

                    <div className="col-md-12">
                        {last_search &&
                            <button className="btn_skill_item" type="button" onClick={e => onClear()}>{last_search} <span><FontAwesomeIcon icon={['fas', 'times']} /></span></button>
                        }
                    </div>
                </div>

            </div>
        </div>


    );
};

export default OfferCondToolbox;